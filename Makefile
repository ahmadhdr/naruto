migrate:
	docker-compose exec app php artisan migrate
deps:
	docker-compose exec app composer install
key:
	docker-compose exec app php artisan key:generate
config:
	cp .env src/.env
seed:
	docker-compose exec app php artisan db:seed
link:
	docker-compose exec app php artisan storage:link