<?php

namespace App\Exports;

use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\WithHeadings;
use Maatwebsite\Excel\Concerns\WithMapping;
use Illuminate\Support\Facades\DB;
use App\Constant\Constant;
use App\Util\Common as CommonUtil;
use App\Util\Transaction;
class DistributionExport implements FromCollection, WithHeadings, WithMapping 
{

	private $user_inputs = [];
	public function __construct($user_inputs)
	{
		$this->user_inputs = $user_inputs;
	}

	public function headings(): array 
	{
		return [
			'Jenis Pembayaran',
			'Jenis Transaksi',
			'Tanggal & Waktu',
			'ID Pesanan',
			'Penerima Dana',
			'Jumlah',
			'Status Transaksi'
		];
	}

	public function map($transaction): array
	{
		return [
			$transaction->payment_name,
			$transaction->transaction_type,
			$transaction->created_at,
			$transaction->order_id,
			$transaction->beneficiary_name,
			CommonUtil::formatAmount($transaction->unit_name, $transaction->paid_amount * -1, 16),
			Transaction::getTransactionNameByTransactionStatus($transaction->transaction_status)];
	}

    /**
    * @return \Illuminate\Support\Collection
    */
    public function collection()
    {
		$query = DB::table('transaction')->select(DB::raw('transaction.id, transaction.order_id, transaction.id_payment_type, payment_type.name as payment_name, transaction.transaction_status, transaction_type.name as transaction_type, transaction.paid_amount, transaction.created_at, unit.name as unit_name, beneficiary.name as beneficiary_name, beneficiary.id as beneficiary_id'))->leftJoin('payment_type', function ($join) {
            $join->on('transaction.id_payment_type', '=', 'payment_type.id');
        })->leftJoin('fund_distribution', function ($join) {
            $join->on('transaction.id', '=', 'fund_distribution.transaction_id'); 
        })->join('beneficiary', function ($join) {
			$join->on('fund_distribution.beneficiary_id', '=', 'beneficiary.id');
		})
		->join('transaction_type', function ($join) {
            $join->on('transaction.id_transaction_type', '=', 'transaction_type.id');
        })->join('unit', function ($join) {
            $join->on('transaction.unit_id', '=', 'unit.id');
        })->where('transaction.id_payment_type', '>', 0)
		->where('transaction.paid_amount', '<=', 0);
        foreach($this->user_inputs as $user_input => $value)
        {
            if (in_array($user_input, ['transaction_end']) && !empty($value))
            {
                if (!empty($user_inputs['transaction_start']))
                {
                    $start = sprintf('%s 00:00:00', $user_inputs['transaction_start']);
                    $end = sprintf('%s 23:59:50', $value);
                    $query->where('transaction.created_at', '>', $start)->where('transaction.created_at', '<', $end);
                }
            }

            if (in_array($user_input, ['nominal_end']) && !empty($value))
            {
                if (!empty($user_inputs['nominal_start']))
                {
                    $start = intval($user_inputs['nominal_start']) * -1;
                    $end = intval($value) * -1; 
                    $query->where('transaction.paid_amount', '>=', $start)->where('transaction.paid_amount', '<=', $end);
                }
            }

			if ($user_input == 'id_beneficiary' && !empty($value))
			{
				$query->where('fund_distribution.beneficiary_id', '=', $value);
			}
			
            if ($user_input == 'transaction_status' && !empty($value))
            {
                $query->where('transaction.transaction_status', '=', $value);
            }

            if ($user_input == 'transaction_type' && !empty($value))
            {
                $query->where('transaction.id_transaction_type', '=', $value);
            }
        }
		return $query->get();
    }
}
